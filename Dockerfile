
# Use the official Go image as a base image
FROM golang:1.19-alpine as builder

# Set the working directory inside the container
WORKDIR /app

# Copy the go mod and sum files to the container
COPY go.mod go.sum ./

# Download all dependencies
RUN go mod download


# Copy the entire project to the container
COPY . .

# Build the Go app
RUN CGO_ENABLED=0 GOOS=linux go build -o main .

# Start a new stage from scratch
FROM alpine:latest

# Set the working directory inside the container
WORKDIR /root/

# Copy the pre-built binary from the previous stage
COPY --from=builder /app/main .

# Copy the env file so osGetenv works (there is probably a better way to do this)
COPY .env .env

# Expose port 8080 for the app (change this if your app uses a different port)
EXPOSE 3000

# Command to run the executable
CMD ["./main"]