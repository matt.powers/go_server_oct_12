package server

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/joho/godotenv"
)

type Server interface {
	LoadEnvironment() error
	GetPort() string
	Start() error
}

type AppServer struct{}

func (s *AppServer) LoadEnvironment() error {
	err := godotenv.Load()
	return err
}

func (s *AppServer) GetPort() string {
	port := os.Getenv("PORT")
	if port == "" {
		port = "3000"
	}
	return ":" + port
}

func (s *AppServer) Start() error {
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "ello")
	})

	port := s.GetPort()
	log.Printf("server listening on port %s", port)

	return http.ListenAndServe(port, handler)
}

func GetAppServer() *AppServer {
	return &AppServer{}
}
