package main

import (
	"log"

	"gitlab.com/matt.powers/go_server_oct_12/server"
)

func main() {
	s := server.GetAppServer()
	if err := s.LoadEnvironment(); err != nil {
		log.Fatal("Error loading env file: ", err)
	}

	if err := s.Start(); err != nil {
		log.Fatal(err)
	}
}
