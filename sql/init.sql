drop schema public cascade;

create schema public

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    username VARCHAR(255) UNIQUE NOT NULL,
    password_hash VARCHAR(255) NOT NULL,
    email VARCHAR(255) UNIQUE,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE categories (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description TEXT
);

CREATE TABLE priorities (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    level INT NOT NULL
);

CREATE TABLE tasks (
    id SERIAL PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    description TEXT,
    due_date TIMESTAMP,
    completed BOOLEAN DEFAULT FALSE,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    user_id INT REFERENCES users(id), -- foreign key if using users table
    category_id INT REFERENCES categories(id), -- foreign key if using categories
    priority_id INT REFERENCES priorities(id) -- foreign key if using priorities
);

CREATE TABLE task_comments (
    id SERIAL PRIMARY KEY,
    comment TEXT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    task_id INT REFERENCES tasks(id),
    user_id INT REFERENCES users(id) -- foreign key if using users table
);

INSERT INTO users (username, password_hash, email)
VALUES
    ('user1', 'password1', 'user1@example.com'),
    ('user2', 'password2', 'user2@example.com'),
    ('user3', 'password3', 'user3@example.com');

INSERT INTO categories (name, description)
VALUES
    ('Work', 'Tasks related to work'),
    ('Personal', 'Personal tasks and chores'),
    ('Study', 'Academic tasks and assignments');

INSERT INTO priorities (name, level)
VALUES
    ('Low', 1),
    ('Medium', 2),
    ('High', 3);

INSERT INTO tasks (title, description, due_date, completed, user_id, category_id, priority_id)
VALUES
    ('Task 1', 'Description for Task 1', '2023-10-20', FALSE, 1, 1, 2),
    ('Task 2', 'Description for Task 2', '2023-10-15', FALSE, 2, 2, 1),
    ('Task 3', 'Description for Task 3', '2023-10-25', TRUE, 1, 3, 3);

INSERT INTO task_comments (comment, task_id, user_id)
VALUES
    ('Comment for Task 1', 1, 1),
    ('Comment for Task 1', 1, 2),
    ('Comment for Task 2', 2, 1),
    ('Comment for Task 3', 3, 2);
